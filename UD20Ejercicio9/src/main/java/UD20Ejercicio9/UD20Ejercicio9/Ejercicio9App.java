package UD20Ejercicio9.UD20Ejercicio9;

import methods.*;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JToggleButton;
import java.awt.Color;

public class Ejercicio9App extends JFrame {

	private JPanel contentPane;
	private final JToggleButton carta1;
	private final JToggleButton carta2;
	private final JToggleButton carta3;
	private final JToggleButton carta4;
	private final JToggleButton carta5;
	private final JToggleButton carta6;
	private final JToggleButton carta7;
	private final JToggleButton carta8;
	private final JToggleButton carta9;
	private final JToggleButton carta10;
	private final JToggleButton carta11;
	private final JToggleButton carta12;
	private final JToggleButton carta13;
	private final JToggleButton carta14;
	private final JToggleButton carta15;
	private final JToggleButton carta16;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio9App frame = new Ejercicio9App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio9App() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 664, 621);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(4, 4, 0, 0));

		carta1 = new JToggleButton("");
		carta1.setSelected(true);
		contentPane.add(carta1);
		carta1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta1.setBackground(Color.RED);
				Methods.flipController(carta1);
			}
		});

		carta2 = new JToggleButton("");
		carta2.setSelected(true);
		contentPane.add(carta2);
		carta2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta2.setBackground(Color.GREEN);
				Methods.flipController(carta2);
			}
		});

		carta3 = new JToggleButton("");
		carta3.setSelected(true);
		contentPane.add(carta3);
		carta3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta3.setBackground(Color.BLUE);
				Methods.flipController(carta3);
			}
		});

		carta4 = new JToggleButton("");
		carta4.setSelected(true);
		contentPane.add(carta4);
		carta4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta4.setBackground(Color.GRAY);
				Methods.flipController(carta4);
			}
		});

		carta5 = new JToggleButton("");
		carta5.setSelected(true);
		contentPane.add(carta5);
		carta5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta5.setBackground(Color.MAGENTA);
				Methods.flipController(carta5);
			}
		});

		carta6 = new JToggleButton("");
		carta6.setSelected(true);
		contentPane.add(carta6);
		carta6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta6.setBackground(Color.ORANGE);
				Methods.flipController(carta6);
			}
		});

		carta7 = new JToggleButton("");
		carta7.setSelected(true);
		contentPane.add(carta7);
		carta7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta7.setBackground(Color.MAGENTA);
				Methods.flipController(carta7);
			}
		});

		carta8 = new JToggleButton("");
		carta8.setSelected(true);
		contentPane.add(carta8);
		carta8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta8.setBackground(Color.PINK);
				Methods.flipController(carta8);
			}
		});

		carta9 = new JToggleButton("");
		carta9.setSelected(true);
		contentPane.add(carta9);
		carta9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta9.setBackground(Color.GREEN);
				Methods.flipController(carta9);
			}
		});

		carta10 = new JToggleButton("");
		carta10.setSelected(true);
		contentPane.add(carta10);
		carta10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta10.setBackground(Color.RED);
				Methods.flipController(carta10);
			}
		});

		carta11 = new JToggleButton("");
		carta11.setSelected(true);
		contentPane.add(carta11);
		carta11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta11.setBackground(Color.GRAY);
				Methods.flipController(carta11);
			}
		});

		carta12 = new JToggleButton("");
		carta12.setSelected(true);
		contentPane.add(carta12);
		carta12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta12.setBackground(Color.YELLOW);
				Methods.flipController(carta12);
			}
		});

		carta13 = new JToggleButton("");
		carta13.setSelected(true);
		contentPane.add(carta13);
		carta13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta13.setBackground(Color.YELLOW);
				Methods.flipController(carta13);
			}
		});

		carta14 = new JToggleButton("");
		carta14.setSelected(true);
		contentPane.add(carta14);
		carta14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta14.setBackground(Color.ORANGE);
				Methods.flipController(carta14);
			}
		});

		carta15 = new JToggleButton("");
		carta15.setSelected(true);
		contentPane.add(carta15);
		carta15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta15.setBackground(Color.BLUE);
				Methods.flipController(carta15);
			}
		});

		carta16 = new JToggleButton("");
		carta16.setSelected(true);
		contentPane.add(carta16);
		carta16.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carta16.setBackground(Color.PINK);
				Methods.flipController(carta16);
			}
		});
	}
}
