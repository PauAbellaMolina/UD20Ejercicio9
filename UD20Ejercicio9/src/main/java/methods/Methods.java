package methods;

import static javax.swing.JOptionPane.showMessageDialog;

import javax.swing.JToggleButton;

public class Methods {
	static JToggleButton selected = null;
	static JToggleButton secondary = null;
	static int correctCounter = 0;
	
	public static void checker(JToggleButton selected, JToggleButton secondary) {
		if (selected.getBackground().equals(secondary.getBackground())) {
			correct(selected, secondary);
		} else {
			reset(selected, secondary);
		}
	}

	public static void flipController(JToggleButton carta) {
		if (selected == null) {
			selected = carta;
		} else if (secondary == null) {
			secondary = carta;
			checker(selected, secondary);
		}
	}

	public static void reset(JToggleButton carta1, JToggleButton carta2) {
		selected = null;
		secondary = null;
		
		carta1.setSelected(true);
		carta1.setBackground(null);
		carta2.setSelected(true);
		carta2.setBackground(null);
	}
	
	public static void correct(JToggleButton carta1, JToggleButton carta2) {
		selected = null;
		secondary = null;
		carta1.setEnabled(false);
		carta2.setEnabled(false);
		correctCounter++;
		if (correctCounter == 8) {
			showMessageDialog(null, "Enhorabuena!");
		}
	}
}
